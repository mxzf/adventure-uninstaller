# Adventure Uninstaller
A small module that adds an Uninstall button to Adventure documents which can remove their contents from the world

![image](documentation/adventure-uninstaller.webp)

## Installation

Module manifest: https://gitlab.com/mxzf/adventure-uninstaller/-/releases/permalink/latest/downloads/module.json
