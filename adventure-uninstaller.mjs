Hooks.on('getAdventureImporterHeaderButtons', (app,buttons) => {
  buttons.unshift({
    class: "adventure-uninstaller",
    icon: "fas fa-trash",
    label: "Uninstall",
    onclick: () => {
      new Dialog({
        title: "Uninstall Adventure",
        content: `<div style="">
        <div style="font-size:1.2em;text-align:center"><strong >Warning!</strong></div>
        <div>This will completely remove all documents which were imported from this adventure.  Any changes you may have made to documents the adventure imported will be<!-- irrecoverably--> lost.  This will not impact anything that wasn't created by the adventure import process.</div><br/>
          <div style="float:left;width:50%"><strong style="">Things that will be removed</strong><br/>(imported from the adventure)<br/>
            <ul>
              <li>Actors</li>
              <li>Cards</li>
              <li>Folders</li>
              <li>Items</li>
              <li>Journals</li>
              <li>Macros</li>
              <li>Playlists</li>
              <li>Roll Tables</li>
              <li>Scenes</li>
            </ul>
          </div>
          <div style="float:left;width:50%"><strong style="">Things that will NOT be removed</strong><br/>
            <ul>
              <li>Documents not from the adventure</li>
              <li>Duplicated adventure documents</li>
              <li>Items added to other Actors</li>
              <li>World settings or flags added/changed by the adventure</li>
            </ul>
          </div>
        </div>`,
        buttons: {
          clean: {
            icon: '<i class="fas fa-trash"></i>',
            label: 'Uninstall',
            callback: async html => {
              let world_docs
              let type_map = {'actors': Actor, 'cards': Cards, 'combats': Combat, 'folders': Folder, 'items': Item, 'journal': JournalEntry, 'macros': Macro, 'playlists': Playlist, 'scenes': Scene, 'tables': RollTable}
              for (let type of Object.keys(type_map)) {
                world_docs = game[type].map(d => d.id)
                await type_map[type].deleteDocuments([...app.adventure[type].filter(d => world_docs.includes(d.id)).map(i => i.id)])
              }
              Object.values(ui.sidebar.tabs).forEach(t => t.render(true))
              app.close()
            },
          },
          cancel: {icon: '<i class="fas fa-times"></i>', label: "Cancel"}
        }
      }, {width: 450}).render(true)}
  })
})