# Changelog

## 0.1.1 - Minor polishing

* Added code to close the adventure import window after uninstalling it
* Added handling to avoid errors when adventure document IDs no longer exist in the world

## 0.1.0 - Initial release

### Features

* Adds an "Uninstall" button to the header bar of an Adventure's import window which can be used to uninstall the adventure by removing its documents.

## 